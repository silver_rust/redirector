{
  # yoinked from https://tulpa.dev/Xe/mara/src/commit/4446edeb050ef24708d03711af998d8d6ac4d998/flake.nix
  # https://github.com/nix-community/naersk/blob/94beb7a3edfeb3bcda65fa3f2ebc48ec6b40bf72/README.md


	description = "A simple rust tool to randomly rickroll";
  
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, utils, naersk }: utils.lib.eachDefaultSystem (system: 
    let
      pkgs = nixpkgs.legacyPackages."${system}";
      naersk-lib = naersk.lib."${system}";
    in rec {

      # `nix build`
      packages.redirector = naersk-lib.buildPackage {
        pname = "redirector";
        root = ./.;
      };

      defaultPackage = packages.redirector;

      # `nix run`
      apps.redirector = utils.lib.mkApp {
        drv = packages.redirector;
      };

      defaultApp = apps.redirector;

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [ rustc cargo ];
      };
      
      nixosModule = { lib, pkgs, config, ... }: 
        with lib; 
        let
          cfg = config.services.redirector;
        in { 
          options.services.redirector = {
            enable = mkEnableOption "enable redirector";
            
            config_file = mkOption rec {
              type = types.str;
              default = "config.toml";
              example = default;
              description = "The path to the config file";
            };
            
            home = mkOption rec {
              type = types.str;
              default = "/etc/silver_redirector";
              example = default;
              description = "The home for the user";
            };
            
            
           user = mkOption rec {
              type = types.str;
              default = "redirector";
              example = default;
              description = "The user to run the service";
            };
            
          };

          config = mkIf cfg.enable {
          
            users.groups."${cfg.user}" = { };
            
            users.users."${cfg.user}" = {
              createHome = true;
              isSystemUser = true;
              home = "${cfg.home}";
              group = "${cfg.user}";
            };
            
            systemd.services."${cfg.user}" = {
              description = "Redirector of links";
              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              wants = [ ];
              unitConfig.ConditionPathExists = cfg.config_file;
              serviceConfig = {
                # fill figure this out in teh future
                #DynamicUser=true;
                User = "${cfg.user}";
                Group = "${cfg.user}";
                Restart = "always";
                ExecStart = "${self.defaultPackage."${system}"}/bin/redirector ${cfg.config_file}";
              };
            };
            
          };
          
        };
      
    });
}