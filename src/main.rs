use rand::seq::SliceRandom;
use serde::Deserialize;
use std::collections::HashMap;
use std::env;
use std::fs;
use std::sync::{Arc, RwLock};
use std::time::Instant;
use tide::{Redirect, Request, Response, StatusCode};

// The shared application state, saves a fuckton of hassles
#[derive(Clone)]
struct State {
    links: Vec<String>,
    counter: Arc<RwLock<HashMap<String, u64>>>,
    start: Instant,
}

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    let args: Vec<String> = env::args().collect();

    let config = if args.len() == 1 { "config.toml" } else { &args[1] };

    // read toml
    let (links_toml, listen_on) = config_get(config);

    // Define a new instance of the state.
    let state = State {
        // process links
        links: links_process(links_toml),
        counter: Default::default(),
        start: Instant::now(),
    };

    tide::log::start();
    let mut app = tide::with_state(state);

    // redirect randomly
    app.at("/").get(links_redirect);
    // favicons were fecking up stats
    app.at("/favicon.ico").get(|_| async { Ok(Response::new(StatusCode::NotFound)) });
    app.at("/stats").get(links_stats);
    app.at("/*path").get(links_redirect);

    app.listen(listen_on).await?;

    Ok(())
}

#[derive(Debug, Deserialize)]
struct Config {
    links: Vec<LinkData>,
    port: String,
}

#[derive(Debug, Deserialize)]
struct LinkData {
    url: String,
    weight: i32,
}

fn config_get(config_path: &str) -> (Vec<LinkData>, String) {
    println!("Searching for '{}'", config_path);
    if let Ok(config_file) = fs::read_to_string(config_path) {
        if let Ok(config) = toml::from_str::<Config>(&*config_file) {
            println!("Found '{}', returning links", config_path);
            // the config file overrides teh defaults
            return (config.links, config.port);
        }
    }

    println!("No '{}' file, using nothing", config_path);
    (vec![], "localhost:8888".to_string())
}

fn links_process(links: Vec<LinkData>) -> Vec<String> {
    let mut link_strings = vec![];
    // items are added the same as their weight.
    // probably many smarter ways to do this but so far its the best one
    for link in links {
        let mut i = 0;
        while i < link.weight {
            link_strings.push(link.url.to_owned());
            i += 1;
        }
    }
    if link_strings.is_empty() {
        // we all know what this is, good song though
        link_strings.push("https://www.youtube.com/watch?v=dQw4w9WgXcQ".to_owned());
    }

    link_strings
}

fn links_rand(links: &[String]) -> String {
    if let Some(link) = links.choose(&mut rand::thread_rng()) {
        link.to_owned()
    } else {
        Default::default()
    }
}

async fn links_redirect(req: Request<State>) -> tide::Result {
    let link = links_rand(&req.state().links);

    let mut counter = req.state().counter.write().unwrap();
    if let Some(x) = counter.get_mut(&link) {
        *x += 1;
    } else {
        counter.insert(link.to_owned(), 1);
    }
    Ok(Redirect::new(link).into())
}

async fn links_stats(req: Request<State>) -> tide::Result {
    let uptime = req.state().start.elapsed();
    let mut response: String = format!("Uptime: {:?}\n", uptime);

    let counter = req.state().counter.read().unwrap();
    for (link, count) in counter.iter() {
        response.push_str(&format!("Link: {} Count: {}\n", link, count));
    }

    Ok(response.into())
}
